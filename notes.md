## Post install

- Add contrib and non-free to sources
- run for 777:

```bash
	sudo apt install -y \
		firmware-linux-nonfree \
		firmware-amd-graphics \
		linux-headers-amd64 \
		broadcom-sta-dkms \
		crda

	sudo modprobe -r b44 b43 b43legacy ssb brcmsmac bcma && sudo  modprobe wl
```

- copy hid_apple.conf to /etc/modprobe.d when using apple keyboard

- run for laptop:

```bash
	sudo apt install -y firmware-iwlwifi xbacklight
```

## Bluetooth

```bash
pulseaudio -k
pulseaudio --start
sudo hciconfig hci0 up - maybe?
```
