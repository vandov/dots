local wezterm = require("wezterm")

local act = wezterm.action
local config = wezterm.config_builder()

config.font = wezterm.font_with_fallback({
	"JetBrains Mono",
	"Noto Color Emoji",
	"Noto Sans Symbols",
	"Nerd Font Symbols",
	"Symbola",
})
config.color_scheme = "GruvboxDark"
config.enable_tab_bar = false
config.window_decorations = "RESIZE"

wezterm.on("update-status", function(window, pane)
	local overrides = window:get_config_overrides() or {}
	if window:is_focused() then
		overrides.window_frame = {
			-- Set the color for each side of the focused window border
			-- border_left_color = "#cc241d", -- Red border for left side when focused
			-- border_top_color = "#cc241d", -- Red border for top side when focused
			-- border_right_color = "#cc241d", -- Red border for right side when focused
			border_bottom_color = "#cc241d", -- Red border for bottom side when focused
			-- border_left_width = 1,
			-- border_right_width = 1,
			-- border_top_height = 1,
			border_bottom_height = 1,
		}
	else
		overrides.window_frame = {}
	end
	window:set_config_overrides(overrides)
end)

wezterm.on("toggle-opacity", function(window, pane)
	local overrides = window:get_config_overrides() or {}
	if not overrides.window_background_opacity then
		overrides.window_background_opacity = 0.5
	else
		overrides.window_background_opacity = nil
	end
	window:set_config_overrides(overrides)
end)

config.keys = {
	{
		key = "B",
		mods = "CTRL",
		action = wezterm.action.EmitEvent("toggle-opacity"),
	},
}

config.mouse_bindings = {
	{
		event = { Down = { streak = 1, button = { WheelUp = 1 } } },
		mods = "CTRL",
		action = act.IncreaseFontSize,
	},
	{
		event = { Down = { streak = 1, button = { WheelDown = 1 } } },
		mods = "CTRL",
		action = act.DecreaseFontSize,
	},
	{
		event = { Up = { streak = 1, button = "Left" } },
		mods = "CTRL",
		action = act.OpenLinkAtMouseCursor,
	},
	{
		event = { Down = { streak = 1, button = "Right" } },
		mods = "NONE",
		action = act.PasteFrom("PrimarySelection"),
	},
}

return config
