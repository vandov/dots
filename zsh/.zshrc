export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$XDG_CONFIG_HOME/local/share"
export XDG_CACHE_HOME="$XDG_CONFIG_HOME/cache"

export HISTFILE="$HOME/.zsh_history"
export HISTSIZE=1000000                   # Maximum events for internal history
export SAVEHIST=1000000                   # Maximum events in history file
setopt SHARE_HISTORY
setopt HIST_IGNORE_ALL_DUPS

export GOPATH=$HOME/go
export GOROOT=/usr/local/go
export GOBIN=$GOPATH/bin
export GOOS=linux
export GOARCH=amd64

export PATH="$PATH:$(du "$HOME/scripts/" | cut -f2 | paste -sd ':')"
export PATH=$PATH:/usr/local/go/bin
export PATH=$PATH:$GOBIN
export PATH=$PATH:/home/vandov/.yarn/bin
export PATH="$PATH:/opt/nvim/"
# export PATH="$(yarn global bin):$PATH"
# export PATH="$HOME/.vector/bin:$PATH"
export LANG=en_US.UTF-8
# You don't strictly need this collation, but most technical people
# probably want C collation for sane results
export LC_COLLATE=C

export EDITOR="nvim"
export TERMINAL="st"
export SHELL="/usr/bin/zsh"
export BROWSER="google-chrome"
export READER="zathura"
export ZSH="$HOME/.oh-my-zsh"
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export PATH=/home/vandov/.local/bin:$PATH

# use direnv extensionextension
eval "$(direnv hook zsh)"

autoload -Uz compinit
compinit

# Load version control information
autoload -Uz vcs_info
zstyle ':vcs_info:*' enable git
# Enable checking for (un)staged changes, enabling use of %u and %c
zstyle ':vcs_info:*' check-for-changes true
# Set custom strings for an unstaged vcs repo changes (*) and staged changes (+)
zstyle ':vcs_info:*' unstagedstr ' *'
zstyle ':vcs_info:*' stagedstr ' +'
# Set the format of the Git information for vcs_info
zstyle ':vcs_info:git:*' formats       '[%b%u%c]'
zstyle ':vcs_info:git:*' actionformats '[%b|%a%u%c]'

autoload -U colors && colors
# chpwd() {
#   vcs_info
# }

function virtualenv_info { 
    [ $VIRTUAL_ENV ] && echo '(vevn:'`basename $VIRTUAL_ENV`') '
}

precmd() {
  #hack to resolve bare repos problem
  vcs_info 2> /dev/null 
  output=$(vcs_info 2>&1)
  if [[ ! -n "$output" ]] && [[ -n ${vcs_info_msg_0_} ]];  then
	  PS1="%{$fg[red]%}┌─╼%{$fg[red]%}[%{$reset_color%}%~% %{$fg[red]%}]%{$reset_color%}%{$reset_color%} %{$fg[red]%}${vcs_info_msg_0_}$(virtualenv_info)%{$reset_color%}
%{$fg[red]%}└────╼%{$reset_color%} "
  else
		PS1="%{$fg[red]%}┌─╼%{$fg[red]%}[%{$reset_color%}%~% %{$fg[red]%}]%{$reset_color%}%{$reset_color%} %{$fg[red]%}$(virtualenv_info)%{$reset_color%}
%{$fg[red]%}└────╼%{$reset_color%} "
  fi
}

_comp_options+=(globdots) # With hidden files
source $HOME/repos/dots/zsh/completion.zsh

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/bin/packer packer

fpath+=~/.zfunc
autoload -Uz compinit && compinit

#comments
setopt interactivecomments

#dirs
setopt auto_pushd
setopt pushd_ignore_dups
setopt pushdminus

alias -g ...='../..'
alias -g ....='../../..'
alias -g .....='../../../..'
alias -g ......='../../../../..'
alias -- -='cd -'
alias 1='cd -1'
alias 2='cd -2'
alias 3='cd -3'
alias 4='cd -4'
alias 5='cd -5'
alias 6='cd -6'
alias 7='cd -7'
alias 8='cd -8'
alias 9='cd -9'
alias l='ls -lah'
alias ls='ls --color=tty'

alias vim="nvim"
alias vi="nvim"
alias v="nvim"
alias grep="grep --color=auto --exclude-dir={.bzr,.git,.hg,.svn,.tox,CVS}"
alias vimdiff='nvim -d'
alias fehs='feh -r -F -V -d -Z -z'
alias top='bpytop'
alias clip="xclip -selection clipboard"
#sxiv -t *.png
alias x="sxiv"
alias vpn_up="wg-quick up wg0"
alias vpn_down="wg-quick down wg0"


# Use emacs key bindings
bindkey -e
# [Home] - Go to beginning of line
if [[ -n "${terminfo[khome]}" ]]; then
  bindkey -M emacs "${terminfo[khome]}" beginning-of-line
  bindkey -M viins "${terminfo[khome]}" beginning-of-line
  bindkey -M vicmd "${terminfo[khome]}" beginning-of-line
fi
# [End] - Go to end of line
if [[ -n "${terminfo[kend]}" ]]; then
  bindkey -M emacs "${terminfo[kend]}"  end-of-line
  bindkey -M viins "${terminfo[kend]}"  end-of-line
  bindkey -M vicmd "${terminfo[kend]}"  end-of-line
fi
# [Backspace] - delete backward
bindkey -M emacs '^?' backward-delete-char
bindkey -M viins '^?' backward-delete-char
bindkey -M vicmd '^?' backward-delete-char
# [Delete] - delete forward
if [[ -n "${terminfo[kdch1]}" ]]; then
  bindkey -M emacs "${terminfo[kdch1]}" delete-char
  bindkey -M viins "${terminfo[kdch1]}" delete-char
  bindkey -M vicmd "${terminfo[kdch1]}" delete-char
else
  bindkey -M emacs "^[[3~" delete-char
  bindkey -M viins "^[[3~" delete-char
  bindkey -M vicmd "^[[3~" delete-char

  bindkey -M emacs "^[3;5~" delete-char
  bindkey -M viins "^[3;5~" delete-char
  bindkey -M vicmd "^[3;5~" delete-char
fi

# [Ctrl-Delete] - delete whole forward-word
bindkey -M emacs '^[[3;5~' kill-word
bindkey -M viins '^[[3;5~' kill-word
bindkey -M vicmd '^[[3;5~' kill-word

# [Ctrl-RightArrow] - move forward one word
bindkey -M emacs '^[[1;5C' forward-word
bindkey -M viins '^[[1;5C' forward-word
bindkey -M vicmd '^[[1;5C' forward-word
# [Ctrl-LeftArrow] - move backward one word
bindkey -M emacs '^[[1;5D' backward-word
bindkey -M viins '^[[1;5D' backward-word
bindkey -M vicmd '^[[1;5D' backward-word

export PNPM_HOME="/home/vandov/.config/local/share/pnpm"
export PATH="$PNPM_HOME:$PATH"
source /usr/share/doc/fzf/examples/key-bindings.zsh
source /usr/share/doc/fzf/examples/completion.zsh

complete -o nospace -C /usr/bin/terraform terraform

# bun completions
[ -s "/home/vandov/.bun/_bun" ] && source "/home/vandov/.bun/_bun"

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"

# fnm
export PATH="/home/vandov/.config/local/share/fnm:$PATH"
eval "`fnm env`"
eval "$(fnm env --use-on-cd)"

_sgpt_zsh() {
if [[ -n "$BUFFER" ]]; then
    _sgpt_prev_cmd=$BUFFER
    BUFFER+=" ⌛"
    zle -I && zle redisplay
    BUFFER=$(sgpt --shell --no-interaction <<< "$_sgpt_prev_cmd")
    zle end-of-line
fi
}
zle -N _sgpt_zsh
bindkey ^l _sgpt_zsh

# pnpm
export PNPM_HOME="/home/vandov/.config/local/share/pnpm"
case ":$PATH:" in
  *":$PNPM_HOME:"*) ;;
  *) export PATH="$PNPM_HOME:$PATH" ;;
esac
# pnpm end
export PATH="$PATH:/opt/mssql-tools18/bin"

# Load pyenv automatically by appending
# the following to
# ~/.bash_profile if it exists, otherwise ~/.profile (for login shells)
# and ~/.bashrc (for interactive shells) :

export PYENV_ROOT="$HOME/.pyenv"
[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

# Restart your shell for the changes to take effect.

# Load pyenv-virtualenv automatically by adding
# the following to ~/.bashrc:

eval "$(pyenv virtualenv-init -)"


# >>>> Vagrant command completion (start)
fpath=(/opt/vagrant/embedded/gems/gems/vagrant-2.4.1/contrib/zsh $fpath)
compinit
# <<<<  Vagrant command completion (end)
